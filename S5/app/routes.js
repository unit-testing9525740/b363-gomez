const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {

		const { name, ex, alias } = req.body;

		if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }

        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
        }

        if (req.body.name === '') { 
		  return res.status(400).send({
		    'error': 'Bad Request - NAME cannot be empty'
		  });
		}

		if(!req.body.hasOwnProperty('ex')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter EX'
            })
        }

        if(typeof req.body.ex !== 'object'){
            return res.status(400).send({
                'error': 'Bad Request - EX has to be an object'
            })
        }

        if (Object.keys(req.body.ex).length === 0) { 
		  return res.status(400).send({
		    'error': 'Bad Request - EX cannot be empty'
		  });
		}

		if(!req.body.hasOwnProperty('alias')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter ALIAS'
            })
        }

        if(typeof req.body.alias !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - ALIAS has to be a string'
            })
        }

        if (req.body.alias === '') { 
		  return res.status(400).send({
		    'error': 'Bad Request - ALIAS cannot be empty'
		  });
		}

	    if (exchangeRates[alias]) {
	        return res.status(400)
	        .send({'error': 'Bad Request - ALIAS already exists'});
	    }else{
	    	return res.status(200)
	    	.send({'message': 'Currency data saved successfully' });
	    }
	    
	});
}
