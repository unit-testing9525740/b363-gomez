const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})
	
	it("test_api_post_currency_is_running", () => {
		chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: 'Philippine Peso',
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	}
		})
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})

	it("test_api_post_currency_returns_200_if_complete_input_given", (done) => {
		chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: 'South Korean Won',
	      	ex:{
		        'peso': 0.043,
		        'usd': 0.00084,
		        'yen': 0.092,
		        'yuan': 0.0059
	      	},
	      	alias: 'dogecoin'
		})
		.end((err, res) => {
            expect (res.status).to.equal(200);
            //console.log(`${res.status} ${JSON.stringify(res.body)}`);
            done();
        });
	})

	it("test_api_post_currency_returns_400_if_no_name", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	}
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            //console.log(`${res.status} ${JSON.stringify(res.body)}`);
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_name_is_not_string", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: true,
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	}
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            //console.log(`${res.status} ${JSON.stringify(res.body)}`);
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_name_is_empty", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: '',
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	}
		})
        .end((err, res) => {
            expect (res.status).to.equal(400); 
            //console.log(`${res.status} ${JSON.stringify(res.body)}`);
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_no_ex", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
	      	name: 'Philippine Peso'
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            //console.log(`${res.status} ${JSON.stringify(res.body)}`);
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_ex_is_not_object", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: 'Philippine Peso',
	      	ex: true,
	      	alias: 'peso'
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            //console.log(`${res.status} ${JSON.stringify(res.body)}`);
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_ex_is_empty", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: 'Philippine Dollar',
	      	ex:{},
	      	alias: 'peso-dollar'
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            //console.log(`${res.status} ${JSON.stringify(res.body)}`);
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_no_alias", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
	      	name: 'Philippine Peso',
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	}
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            //console.log(`${res.status} ${JSON.stringify(res.body)}`); 
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_alias_is_not_string", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: 'Philippine Peso',
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	},
	      	alias: true
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            //console.log(`${res.status} ${JSON.stringify(res.body)}`);
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_alias_is_empty", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: 'Philippine Peso',
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	},
	      	alias: ''
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            //console.log(`${res.status} ${JSON.stringify(res.body)}`);
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_duplicate_alias", (done) => {
	    // Define an alias that already exists
	    const existingAlias = 'peso';

	    chai.request('http://localhost:5001')
	    .post('/currency')
	    .type('json')
	    .send({
	        name: 'New Peso',
	        ex: {
	            'usd': 0.020,
	            'won': 23.39,
	            'yen': 2.14,
	            'yuan': 0.14
	        },
	        alias: existingAlias 
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	        //console.log(`${res.status} ${JSON.stringify(res.body)}`);
	        done();
	    });
	});

	it("test_api_post_currency_returns_200_if_no_duplicates", (done) => {
	    chai.request('http://localhost:5001')
	    .post('/currency') 
	    .type('json')
	    .send({ 
			name: 'BitCoin',
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	},
	      	alias: 'bitcoin'
		})
	    .end((err, res) => {
	        expect(res.status).to.equal(200); 
	        //console.log(`${res.status} ${JSON.stringify(res.body)}`);
	        done();
	    })
    })

})