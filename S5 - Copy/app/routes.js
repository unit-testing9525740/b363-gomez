const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {

		if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }

        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
        }

        if (req.body.name === '') { 
		  return res.status(400).send({
		    'error': 'Bad Request - NAME cannot be empty'
		  });
		}

		if(!req.body.hasOwnProperty('ex')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter EX'
            })
        }

        if(typeof req.body.ex !== 'object'){
            return res.status(400).send({
                'error': 'Bad Request - EX has to be an object'
            })
        }

        if (req.body.ex === '') { 
		  return res.status(400).send({
		    'error': 'Bad Request - EX cannot be empty'
		  });
		}

		if(!req.body.hasOwnProperty('alias')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter EX'
            })
        }

        if(typeof req.body.alias !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - EX has to be an object'
            })
        }

        if (req.body.alias === '') { 
		  return res.status(400).send({
		    'error': 'Bad Request - EX cannot be empty'
		  });
		}

		const usedAliases = [];

		if (usedAliases.includes(req.body.alias)) {
        return res.status(400).json({ message: 'Duplicate alias found' });
		} 
		else {
        return res.status(200).json({ message: 'No duplicate alias found' });
    	}

		return res.status(200).send({
		    'message': 'Currency data saved successfully'
		});

	});
}
