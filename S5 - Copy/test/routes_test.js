const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
			console.log(res.status); 
            console.log(res.body); 
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			console.log(res.status); 
            console.log(res.body); 
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			console.log(res.status); 
            console.log(res.body); 
			done();	
		})		
	})
	
	it("test_api_post_currency_is_running", () => {
		chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: 'Philippine Peso',
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	}
		})
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
			console.log(res.status); 
            console.log(res.body); 
		})
	})

	it("test_api_post_currency_returns_200_if_complete_input_given", (done) => {
		chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			
			name: 'Philippine Peso',
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	},
	      	alias: 'peso'
		})
		.end((err, res) => {
			//console.log(res.status); 
            //console.log(res.body); 
            expect (res.status).to.equal(200);
            console.log(res.status); 
            console.log(res.body); 
            done();
        });
	})

	it("test_api_post_currency_returns_400_if_no_name", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	}
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            console.log(res.status); 
            console.log(res.body); 
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_name_is_not_string", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: true,
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	}
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            console.log(res.status); 
            console.log(res.body); 
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_name_is_empty", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: '',
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	}
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            console.log(res.status); 
            console.log(res.body); 
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_no_ex", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
	      	name: 'Philippine Peso'
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            console.log(res.status); 
            console.log(res.body); 
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_ex_is_not_object", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: 'Philippine Peso',
	      	ex: true
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            console.log(res.status); 
            console.log(res.body); 
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_ex_is_empty", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: 'Philippine Peso',
	      	ex:''
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            console.log(res.status); 
            console.log(res.body); 
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_no_alias", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
	      	name: 'Philippine Peso',
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	}
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            console.log(res.status); 
            console.log(res.body); 
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_alias_is_not_string", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: 'Philippine Peso',
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	},
	      	alias: true
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            console.log(res.status); 
            console.log(res.body); 
            done();
        });
    })

    it("test_api_post_currency_returns_400_if_alias_is_empty", (done) => {
        chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			name: 'Philippine Peso',
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
	      	},
	      	alias: ''
		})
        .end((err, res) => {
            expect (res.status).to.equal(400);
            console.log(res.status); 
            console.log(res.body); 
            done();
        });
    })

    /*it("test_api_post_currency_returns_400_if_there_are_duplicate_alias", (done) => {
		chai.request('http://localhost:5001')
		.post('/currency') 
		.type('json')
		.send({ 
			alias: 'peso',
			name: 'Philippine Peso',
	      	ex:{
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14,
	      	},
		})
		.end((err, res) => {
			console.log(res.status); // Log the status code
            console.log(res.body);   // Log the response body
            expect (res.status).to.equal(400);
            done();
        });
	})*/

	it("test_api_post_currency_returns_200_if_no_duplicates", (done) => {
	    // First, send a request with a unique alias
	    chai.request('http://localhost:5001')
	    .post('/currency') 
	    .type('json')
	    .send({ 
	        alias: 'unique_alias',  // Unique alias
	        name: 'Some Currency',
	        ex: {
	            'usd': 0.020,
	            'won': 23.39,
	            'yen': 2.14,
	            'yuan': 0.14,
	        },
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(200); 
	        console.log(res.status); 
            console.log(res.body); 
	        done();
	    })
    })

    it("test_api_post_currency_returns_400_if_duplicate_alias_found", (done) => {
    // Send a request with unique aliases
    chai.request('http://localhost:5001')
    .post('/currency') 
    .type('json')
    .send(
        { 
            alias: 'unique_alias_1',  
            name: 'Currency 1',
            ex: {
                'usd': 0.030,
                'won': 24.39,
                'yen': 2.24,
                'yuan': 0.15,
            }
        }
    )
/*    .end((err, res) => {
        console.log(res.status); // Log the status code
        console.log(res.body);   // Log the response body
        expect(res.status).to.equal(200);  // Expect 200 because aliases are unique*/

        // Send another request with a duplicate alias
        chai.request('http://localhost:5001')
        .post('/currency') 
        .type('json')
        .send(
            { 
                alias: 'unique_alias_1',  // Duplicate alias
                name: 'Currency 3',
                ex: {
                    'usd': 0.030,
                    'won': 24.39,
                    'yen': 2.24,
                    'yuan': 0.15,
                }
            }
        )
        .end((err, res) => {
            expect(res.status).to.equal(400);
            console.log(res.status); 
            console.log(res.body); 
            done();
        });
    });
});





