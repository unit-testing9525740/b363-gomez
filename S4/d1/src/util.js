function factorial(n){
  if(isNaN(n)) return undefined;
  if(typeof n !== "number") return undefined;
  if(n<0) return undefined; // Green - write code to pass the test
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
	// 5 * 4 * 3 * 2 * 1 = 120
} 

function div_check(num) {
  if (num % 5 === 0 || num % 7 === 0) {
    return true;
  } else {
    return false
  }
}

//sol 2
/*
function div_check(num) {
  if (num % 5 === 0 return true;
  if (num % 7 === 0 return true;
    return false
}
*/

const names = {
  "Ian":{ "name": "Ian Gomez", "age": 35},
  "Theo":{ "name": "Theo Pogi", "age": 8}
}

// S4 Activity Start
const users = [
    {
        username: "brBoyd87",
        password: "87brandon19"

    },
    {
        username: "tylerofsteve",
        password: "stevenstyle75"
    }
]

module.exports = {
  factorial: factorial,
  div_check: div_check,
  names: names,
  users: users
};
