function factorial(n){
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
	// 5 * 4 * 3 * 2 * 1 = 120
} 

function div_check(num) {
  if (num % 5 === 0 || num % 7 === 0) {
    return true;
  } else {
    return false
  }
}

//sol 2
/*
function div_check(num) {
  if (num % 5 === 0 return true;
  if (num % 7 === 0 return true;
    return false
}
*/

const names = {
  "Ian":{ "name": "Ian Gomez", "age": 35},
  "Theo":{ "name": "Theo Pogi", "age": 8}
}


module.exports = {
  factorial: factorial,
  div_check: div_check,
  names: names
};