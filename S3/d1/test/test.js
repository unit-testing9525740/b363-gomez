const { factorial, div_check} = require('../src/util.js');

// Gets the expect and assert functions from chai
const { expect, assert} = require('chai');

// Test Suites are made up of collection of test cases that should be executed together

// "describe" keyword is used to group tests together
describe('test_fun_factorials', () => {

	// "it()" accepts two parameters
	// 1. string explaining what the test should do
	// 2. callback function which contains the actual test

	it('the factorial of 5 is 120', () => {
		const product = factorial(5);
		// "expect" - returning expected and actual value
		expect(product).to.equal(120);
	})

	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		//"assert" - checking if the function returns the correct result
		assert.equal(product, 120);
	})

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1);
	})

	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0);
		assert.equal(product, 1);
	})

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4);
		expect(product).to.equal(24);
	})

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10);
		expect(product).to.equal(3628800);
	})

})


describe('test_divisibility_by_5_or_7', () => {

	it('test_100_is_divisible_by_5', () => {
		const modulo = div_check(100);
		assert.equal(modulo, true);
	})

	it('test_49_is_divisible_by_7', () => {
		const modulo = div_check(49);
		assert.equal(modulo, true);
	})

	it('test_30_is_divisible_by_5', () => {
		const modulo = div_check(30);
		assert.equal(modulo, true);
	})

	it('Testing if 56 is divisible by 7', () => {
		const modulo = div_check(56);
		assert.equal(modulo, true);
	})

	it('Testing if 56 is divisible by 7', () => {
		assert.equal(div_check(56), true);
	})
})